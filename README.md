**Monolith**

*A game about knowing nothing.*

Monolith is a puzzle game with rpg-like elements; in this game, you take the place of an unknown protagonist on an unknown realm. Your goal, motives, reasons, abilities, they're all unknown, the only thing you know is the Monolith, who will guide you through your journey, but, your journey for what?
What are you doing here?
What is the Monolith talking about?
And why is he talking... like that?
Only one way to find out, there's not much time left, so you better hurry.


## Requirements


- OS: Windows 7 SP1+, macOS 10.12+  
- Graphics card with DX10 (shader model 4.0) capabilities.​  
- CPU: SSE2 instruction set support.​
- Memory: 4 GB RAM

##We have no information about controls and cannot provide you with any further support. Sorry and have fun!

##Source code aid...

If you want to know more about this code, we'd be more than happy to hear about you!
Write to us: info@applicate.co