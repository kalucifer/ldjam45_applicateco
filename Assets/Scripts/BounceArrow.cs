﻿using System;
using UnityEngine;

public class BounceArrow : MonoBehaviour
{
    public bool izquierda;
    public bool derecha;
    public bool arriba;

    public bool abajo;
    SFX SFX;

    void Start() 
    {
        SFX = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<SFX>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Arrow"))
        {
            SFX.Sound(SFX.sfx_arror_Bounce);
            Vector3 velocity = collision.gameObject.GetComponent<Rigidbody2D>().velocity;
            Transform transformArrow = collision.gameObject.GetComponent<Transform>();

            Quaternion rotationCollision = collision.gameObject.GetComponent<Transform>().rotation;

            float velocityArrow = collision.gameObject.GetComponent<Arrow>().velocity;

           // if (shield.lastActivate == 3 || shield.lastActivate == 2)
            //{
                collision.gameObject.GetComponent<Transform>().eulerAngles =
                    new Vector3(0, 0, -rotationCollision.eulerAngles.z);
            //}

            /*if (shield.lastActivate == 1 || shield.lastActivate == 0)
            {
                collision.gameObject.GetComponent<Transform>().eulerAngles =
                    new Vector3(rotationCollision.eulerAngles.x + 180, 0, rotationCollision.eulerAngles.z);
            }*/

            collision.gameObject.GetComponent<Rigidbody2D>().velocity =
                collision.gameObject.GetComponent<Transform>().up * velocityArrow;

        }
    }
}
