﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
	public float FollowSpeed = 3f;
	public Movement Target;
	public float offset = -10;

	private void Update()
	{
		Vector3 newPosition = Target.gameObject.transform.position;
		newPosition.z = -offset;
		if(Target.direction.Equals(0))
			newPosition.x += offset;
		else if(Target.direction.Equals(1))
			newPosition.x -= offset;
		else if(Target.direction.Equals(2)) 
			newPosition.y += offset;
		else if(Target.direction.Equals(3)) 
			newPosition.y -= offset;
		transform.position = Vector3.Slerp(transform.position, newPosition, FollowSpeed * Time.deltaTime);
		
	}
}
