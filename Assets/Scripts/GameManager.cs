﻿using System;
using UnityEngine;

[RequireComponent(typeof(Strings))]
public class GameManager : MonoBehaviour
{
    public Strings strings;
    private sceneChangerClick sceneChangerClick;
    
    public bool gameOnProgress;
    public int levelNumber;

    private void Awake()
    {
        gameOnProgress = PlayerPrefs.GetInt("levelNumber") > 0 ? true : false;
        strings = GetComponent<Strings>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        
        sceneChangerClick = GameObject.Find("Canvas").GetComponent<sceneChangerClick>();

        if (gameOnProgress)
        {
            levelNumber = PlayerPrefs.GetInt("levelNumber");
            print("Game On Progress");
            print("LevelNumber Player Prefs: " + PlayerPrefs.GetInt("levelNumber"));
            print("LevelNumber variable: " + levelNumber);
        }
        else
        {
            PlayerPrefs.SetInt("levelNumber", levelNumber);
            print("New Game started");
        }

    }

    public void newGame()
    {
        levelNumber = 0;
        PlayerPrefs.SetInt("levelNumber", levelNumber);
        PlayerPrefs.Save();
        sceneChangerClick.irAEscena("2Room");
    }

    public void LoadGame()
    {
        levelNumber = PlayerPrefs.GetInt("levelNumber");
        sceneChangerClick.irAEscena(strings.escenas[levelNumber]);
    }

    public void SaveGame()
    {
        PlayerPrefs.SetInt("levelNumber", levelNumber+1);
        PlayerPrefs.Save();
    }
}
