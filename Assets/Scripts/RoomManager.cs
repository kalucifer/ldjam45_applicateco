﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    public GameObject frame;
    SFX SFX;
    AudioSource Reductor;

    public TMP_Text dialog;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(help());
        SFX = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<SFX>();
        Reductor = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<AudioSource>();
    }

    private IEnumerator help()
    {
        yield return new WaitForSeconds(5);
        frame.SetActive(true);
        var textDialog = dialog.GetComponent<TMP_Text>();
        string textTodisplay = "F1gHt uZing P k3Y.";
        foreach (char c in textTodisplay) 
        {
            textDialog.text += c;
            if(!c.Equals( ' '))
                SFX.Sound(SFX.sfx_typing);
            yield return new WaitForSeconds (0.11f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
