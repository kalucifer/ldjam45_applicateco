﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowSpawner : MonoBehaviour
{
    public GameObject arrow;
    public GameObject player;

    public float timeToStart;
    public float timeToSpawn;

    SFX SFX;

    // Start is called before the first frame update
    void Start()
    {
        SFX = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<SFX>();
        InvokeRepeating("Shoot", timeToStart, timeToSpawn);
    }

    void Shoot()
    {
        SFX.Sound(SFX.sfx_arrow_Shoot);
        Vector2 targetPos = player.transform.position;
        Vector2 thisPos = transform.position;
        targetPos.x = targetPos.x - thisPos.x;
        targetPos.y = targetPos.y - thisPos.y;
        float angle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;

        Instantiate(arrow, transform.position, Quaternion.Euler(new Vector3(0, 0, angle - 90)));
    }
}
