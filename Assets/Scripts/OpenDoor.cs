﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    private Animator animator;

    private BoxCollider2D colliderr;

    public Boolean doorOpened;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        colliderr = GetComponent<BoxCollider2D>();
    }

    public void Open()
    {
        colliderr.enabled = false;
        animator.SetBool("Abrir", true);
        animator.SetBool("Cerrar", false);
        doorOpened = true;
    }
    
    public void Close()
    {
        colliderr.enabled = true;
        animator.SetBool("Cerrar", true);
        animator.SetBool("Abrir", false);
        doorOpened = false;
    }
}
