﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour
{

    private GameManager gameManager;

    public GameObject botonContinue;
    public TMP_Text texto;
    
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindWithTag("GameController").GetComponent<GameManager>();
        if (gameManager.gameOnProgress)
        {
            print("Titulo: puede continuar");
            botonContinue.GetComponent<Button>().interactable = true;
        }

        if (gameManager.levelNumber.Equals(5))
        {
            print("Titulo: reiniciando juego");
            PlayerPrefs.SetInt("levelNumber",0);
        }
            
    }

    private void Update()
    {
        if (gameManager.levelNumber > 0)
            texto.SetText(gameManager.strings.ingameText[gameManager.levelNumber - 1]);
    }
}
