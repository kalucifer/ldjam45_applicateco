﻿using UnityEngine;

public class DoorOpener : MonoBehaviour
{
    private GameObject particleSystemm;

    public bool open = true;

    SFX SFX;
    // Start is called before the first frame update
    void Start()
    {
        SFX = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<SFX>();
        particleSystemm = transform.GetChild(0).gameObject;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Arrow"))
        {
            particleSystemm.GetComponent<ParticleSystem>().Play();
            //animar
            
            if (open)
            {
                GameObject.FindWithTag("Obstacle").GetComponent<OpenDoor>().Open();
                SFX.Sound(SFX.sfx_mineral_Open);
            }
            else
            {
                GameObject.FindWithTag("Obstacle").GetComponent<OpenDoor>().Close();
                SFX.Sound(SFX.sfx_mineral_Close);
            }
        }
    }
}
