﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsManager : MonoBehaviour
{
    private sceneChangerClick sceneChangerClick;
    // Start is called before the first frame update
    void Start()
    {
        sceneChangerClick = GameObject.Find("Canvas").GetComponent<sceneChangerClick>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P) || Input.GetMouseButtonDown(0))
            sceneChangerClick.irAEscena("1Title");
    }
}
