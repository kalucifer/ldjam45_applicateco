﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldTrigger : MonoBehaviour
{
    public Rigidbody2D rb;

    public Shield shield;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("Object detected");


        if (collision.tag.Equals("Arrow"))
        {
            Vector3 velocity = collision.gameObject.GetComponent<Rigidbody2D>().velocity;
            Transform transformArrow = collision.gameObject.GetComponent<Transform>();

            Quaternion rotationCollision = collision.gameObject.GetComponent<Transform>().rotation;

            float velocityArrow = collision.gameObject.GetComponent<Arrow>().velocity;

            if (shield.lastActivate == 3|| shield.lastActivate == 2)
            {
                collision.gameObject.GetComponent<Transform>().eulerAngles = new Vector3(0, 0, -rotationCollision.eulerAngles.z);
            }

            if (shield.lastActivate == 1 || shield.lastActivate == 0)
            {
                collision.gameObject.GetComponent<Transform>().eulerAngles = new Vector3(rotationCollision.eulerAngles.x + 180, 0, rotationCollision.eulerAngles.z);
            }
            /* else
             {
                 if (velocity.x < 0)
                     collision.gameObject.GetComponent<Transform>().eulerAngles = new Vector3(0, 0, rotationCollision.eulerAngles.z - 90);
                 else
                     collision.gameObject.GetComponent<Transform>().eulerAngles = new Vector3(0, 0, rotationCollision.eulerAngles.z + 90);

             }*/
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = collision.gameObject.GetComponent<Transform>().up * velocityArrow;

        }


    }
}
