﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    public AudioClip sfx_arror_Bounce;
    public AudioClip sfx_arrow_Shoot;
    public AudioClip sfx_arrow_Missed;

    public AudioClip sfx_brokenPlate;

    public AudioClip sfx_mineral_Close;
    public AudioClip sfx_mineral_Open;

    public AudioClip sfx_playerHurted;

    public AudioClip sfx_sword_attack01;
    public AudioClip sfx_sword_attack02;
    public AudioClip sfx_sword_wall;

    public AudioClip sfx_Crashed;
    public AudioClip sfx_typing;

    public AudioSource audioSource;

    private bool canPlay = true;


    public void Sound(AudioClip A)
    {
        audioSource.clip = A;

        if (canPlay) 
        {
            audioSource.Play(); 
            canPlay = false;
        }
        if (!canPlay) 
        {
            canPlay = true;
        }
        
    }

}
