﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VesselDestroyer : MonoBehaviour
{
    private SFX sfx;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Sword"))
        {
            sfx.Sound(sfx.sfx_brokenPlate);
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<Collider2D>().enabled = false;

            gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        sfx = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<SFX>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
