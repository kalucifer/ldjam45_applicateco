﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class NotesWriter : MonoBehaviour
{

    public string textName;
    public string TextoToWrite;

    private string path;
    private void Start()
    {
        //path = "C:/Users/" + Environment.UserName + "/Desktop/" + textName;
        //path = "E:/Documents/Desktop/" + textName;
        //path = Application.platform.Equals(RuntimePlatform.WindowsPlayer) ? "C:/Users/" + Environment.UserName + "/Desktop/" + textName : Application.dataPath;
        //path = Path.Combine(Application.dataPath, textName);
        //path = Application.dataPath + "/" + textName;
        //string pathString = @"c:\Prueba\SubFolder2";
        //System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "/Monolith_Directory");
        //path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "/" + textName;
        path = "";
    }

    public void CreateText()
    {
        path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "/" + textName;
        print("TextName: " + textName);
        print("Path: " + path);

        if (!File.Exists(path))
        {
            File.WriteAllText(path, "");
        }
        File.AppendAllText(path, TextoToWrite);
    }
}
