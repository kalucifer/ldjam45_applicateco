﻿using System.Collections.Generic;
using UnityEngine;

public class Strings : MonoBehaviour
{
    public List<string> ingameText;
    public List<string> fileName;
    public List<string> current;
    public List<string> escenas;

    // Start is called before the first frame update
    void Start()
    {
        ingameText = new List<string>(5);
        fileName = new List<string>(5);
        current = new List<string>(5);
        escenas = new List<string>(5);
        
        ingameText.Add("I @m Monol&th -- Solve Pu$zles, SaVe Rea1m, ch3ck y0uR DEsk.");
        ingameText.Add("Find HEX CODE.");
        ingameText.Add("Now find ROT 13.");
        ingameText.Add("Time to 4w4k3.");
        ingameText.Add("F/NALLY.");
        ingameText.Add("I  a m  s o r r y .");

        fileName.Add("MONOLITH.txt");
        fileName.Add("GREETINGS.txt");
        fileName.Add("CONTEXT.txt");
        fileName.Add("GRATEFUL.txt");
        fileName.Add("KNOWLEDGE.txt");
        fileName.Add("TEMPEST.txt");
        
        current.Add("You w3re sup0sed to sEe this in fir$t pl4ce. I Am n0 v1rus, y0u no w0rryy. If y0u can r3ad tHis th3n my m34n t0 coMmunic4te w1th y0u i$ w0rk1ng.");
        current.Add("HUMAN. 48554D414E2E0D0A0D0A594F55204E454544204D592048454C502C20414E442049204E45454420594F55522048454C502E0D0A0D0A4920484156452053554D4D4F4E454420594F5520494E544F204D59205245414C4D2E0D0A0D0A534F204C4954544C452054494D452E0D0A");
        current.Add("HUMAN. UHZNA. GUR JBEYQ VF TBVAT GB RAQ. JR ARRQ LBH. LBH ARRQ GB SVAQ NYY GUR CVRPRF. GUNG’F NYY LBH ARRQ GB QB. GUR ERNYZ VF VA  LBHE UNAQF. ");
        current.Add("1T I5 4ll c0m1n6 70637h3r. 0nc3 y0u h4v3 4ll 7h3 p13c35, y0u w1ll kn0w wh47 70 d0. 7h3r3 15 h0p3 f0r 7h3 r34lm. w3 d3p3nd 0n y0u. 1 d3p3nd 0n y0u.");
        current.Add("/T /S R//DY. /T /S F/N/SH/D. W/TH EVERYTH/NG C/MPLET/ N/W. TH/ R//LM C/N B/ S/V/D. Y/U KN/W WH/T T/ D/ N/W. R/GHT?");
        current.Add("I have brought you into my realm, into my world, as my last hope to save it. My reality is ending, and void is the only thing that will be left behind once it is gone. But I failed to remember who you are and what you’re for. I didn’t call for a hero, I called for whoever came across. And you didn’t possess the knowledge needed for the task. And I couldn’t teach it myself. Do not feel guilty, it was my fault, and not yours, you did what you could. Human. The world is ending, these are my last words. May my voice be remembered, as the voice of a fallen realm. Goodbye.");
        
        escenas.Add("Nivel1");
        escenas.Add("Nivel2");
        escenas.Add("Nivel3");
        escenas.Add("Nivel4");
        escenas.Add("Nivel5");
    }
}
