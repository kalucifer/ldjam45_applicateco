﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Shield : MonoBehaviour
{

    public List<PolygonCollider2D> colliders = new List<PolygonCollider2D>();

    public Animator animator;

    public Rigidbody2D rb;

    public GameObject shield;

    public float animationTime;

    public KeyCode keyShield;

    public int lastActivate = 0;

    private bool destroyVesel;

    SFX SFX;
    private bool changeSound;

    private void Start()
    {
        SFX = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<SFX>();
        shield.SetActive(false);
    }

    void Update()
    {
        float dirX = Input.GetAxis("Horizontal");
        float dirY = Input.GetAxis("Vertical");

        for (int i = 0; i < colliders.Count; i++)
        {
            if (colliders[i].enabled)
                lastActivate = i;
        }

        if (dirY > 0)
            colliders[0].enabled = true;
        else if (dirY < 0)
            colliders[1].enabled = true;
        else if (dirX > 0)
            colliders[2].enabled = true;
        else if (dirX < 0)
            colliders[3].enabled = true;
        else
            colliders[lastActivate].enabled = true;
  

        if (Input.GetKeyDown(keyShield))
        {
            //Cambiamos el sonido para no dejar el mismo para el machetaso
            //que buena ortografía kaleth att sebas
            
            TurnOnShield();
            if (changeSound)
            {
                SFX.Sound(SFX.sfx_sword_attack01);
                changeSound = false;
            }
            else 
            {
                SFX.Sound(SFX.sfx_sword_attack02);
                changeSound = true;
            }
            
        }

        void TurnOnShield()
        {
            destroyVesel = true;
            shield.SetActive(true);
            

            for (int i = 0; i < colliders.Count; i++)
            {
                colliders[i].enabled = false;
            }

            StartCoroutine(TurnOffShield());
        }


    }

    IEnumerator TurnOffShield()
    {
        animator.SetTrigger("Espada");
        yield return new WaitForSeconds(animationTime);
        destroyVesel = false;
        shield.SetActive(false);
    }

    public void DiePlayer()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
