﻿using UnityEngine;

public class FirstRoom : MonoBehaviour
{
    private sceneChangerClick sceneChangerClick;
    private void OnCollisionEnter2D(Collision2D other)
    {
        sceneChangerClick.irAEscena("Nivel1");
    }

    // Start is called before the first frame update
    void Start()
    {
        sceneChangerClick = GetComponent<sceneChangerClick>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
