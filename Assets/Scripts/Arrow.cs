﻿using UnityEngine;

public class Arrow : MonoBehaviour
{
    public Rigidbody2D rb;
    public float velocity;

    SFX SFX;

    void Start()
    {
        SFX = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<SFX>();
        rb.velocity = gameObject.transform.up * velocity;
        Invoke("KillMe", 30f);
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
<<<<<<< HEAD
        if (collision.gameObject.tag.Equals("Player"))
        {

            collision.gameObject.GetComponent<Movement>().Kill = true;
            Destroy(this.gameObject);
        }
    }

    void KillMe()
    {
        Destroy(this.gameObject);
=======
        SendMessage("DiePlayer");
        Destroy(gameObject);
    }*/

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            SFX.Sound(SFX.sfx_playerHurted);
            GameObject.FindWithTag("Player").GetComponent<Movement>().Kill = true;
            KillMe();
        }
        else if (collision.gameObject.CompareTag("Open"))
        {
            GameObject.FindWithTag("Open").GetComponent<DoorOpener>();
            KillMe();
        }
         else if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Obstacle") || collision.gameObject.CompareTag("Close"))
        {
            SFX.Sound(SFX.sfx_arrow_Missed);
            KillMe();
        }
        else if (collision.gameObject.CompareTag("Breakable"))
        {
            SFX.Sound(SFX.sfx_brokenPlate);
            KillMe();
            collision.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            collision.gameObject.GetComponent<Collider2D>().enabled = false;

            collision.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;

        }
    }

    void KillMe()
    {
        Destroy(gameObject);
    }

}


