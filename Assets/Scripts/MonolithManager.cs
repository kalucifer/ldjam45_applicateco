﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MonolithManager : MonoBehaviour
{
    private GameManager gameManager;
    private sceneChangerClick sceneChangerClick;

    public float timeToTalk;
    private bool canPass;

    public GameObject frame;
    public GameObject dialog;
    private Strings strings;

    public float timeToCrashed;
    public GameObject Camara;

    SFX SFX;
    AudioSource Reductor;

    // Start is called before the first frame update
    void Start()
    {
        SFX = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<SFX>();
        Reductor = GameObject.FindGameObjectWithTag("SFXcontroller").GetComponent<AudioSource>();
        canPass = false;
        gameManager = GameObject.FindWithTag("GameController").GetComponent<GameManager>();
        sceneChangerClick = GameObject.Find("Canvas").GetComponent<sceneChangerClick>();
        strings = gameManager.strings;
        StartCoroutine("talk");
    }

    private IEnumerator talk()
    {
        yield return new WaitForSeconds(timeToTalk);
        frame.SetActive(true);
        var textDialog = dialog.GetComponent<TMP_Text>();
        string textTodisplay = strings.ingameText[gameManager.levelNumber - 1] + " (Presz KEy to m0ve on).";
        foreach (char c in textTodisplay) 
        {
            textDialog.text += c;
            if(!c.Equals( ' '))
                SFX.Sound(SFX.sfx_typing);
            yield return new WaitForSeconds (0.11f);
        }

        canPass = true;
        print("you can go now");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && canPass)
        {
            canPass = false;
            //sceneChangerClick.irAEscena(strings.escenas[gameManager.levelNumber]);
            Camara.GetComponent<AudioSource>().enabled = false;
            var writer = GetComponent<NotesWriter>();
            writer.textName = strings.fileName[gameManager.levelNumber-1];
            writer.TextoToWrite = strings.current[gameManager.levelNumber-1];
            writer.CreateText();
            Reductor.volume = 1.0F;
            StartCoroutine("Crashed");
            SFX.Sound(SFX.sfx_Crashed);
        }
    }
    private IEnumerator Crashed() 
    {
        yield return new WaitForSeconds(timeToCrashed);
        if(gameManager.levelNumber.Equals(5))
            sceneChangerClick.irAEscena("Credits");
        else
        {
            PlayerPrefs.Save();
            Application.Quit();
        }
    }
}
