﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
    public Animator animator;
    public Rigidbody2D rb;

    public float movementVelocity;
    public float smoothTime;

    public bool Kill = false;
    bool Destroying = false;

    public SpriteRenderer characterSprite;

    private Vector2 velRef;
    private Vector3 _startPos;

    [HideInInspector]
    public int direction;

    // Start is called before the first frame update
    void Start()
    {
        velRef = Vector2.zero;
        _startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float Hor = Input.GetAxis("Horizontal");
        float Ver = Input.GetAxis("Vertical");

        if (Hor > 0)
            direction = 0;
        else if (Hor < 0)
            direction = 1;
        else if (Ver > 0)
            direction = 2;
        else if (Ver < 0)
            direction = 3;

        Move(Hor, Ver);
        Rotate();
        Animate();

        if (Kill && !Destroying)
            StartCoroutine(DestroyCharacter());
    }

    void Move(float Hor, float Ver)
    {
        Vector2 newVelocity = Vector2.SmoothDamp(rb.velocity, new Vector2(Hor, Ver) * movementVelocity, ref velRef, smoothTime);

        rb.velocity = newVelocity;
    }

    void Rotate()
    {
        if (rb.velocity.x < 0)
            characterSprite.flipX = true;
        else if (rb.velocity.x > 0)
            characterSprite.flipX = false;
    }

    void Animate()
    {
        animator.SetFloat("Speed", rb.velocity.magnitude);
    }

    public IEnumerator DestroyCharacter()
    {
        Destroying = true;
        animator.SetBool("Morir", true);
        yield return new WaitForSeconds(0.4f);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        transform.position = _startPos;
        animator.SetBool("Morir", false);
        Destroying = false;
        Kill = false;
    }
}
