﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IncrementLevel : MonoBehaviour
{
    private GameManager gameManager;
    private OpenDoor openDoor;
    private sceneChangerClick sceneChangerClick;

    private void Start()
    {
        gameManager = GameObject.FindWithTag("GameController").GetComponent<GameManager>();
        openDoor = GameObject.FindWithTag("Obstacle").GetComponent<OpenDoor>();
        print("Game controller: " + gameManager);
        print("Door: " + openDoor.doorOpened);
        sceneChangerClick = GameObject.Find("Canvas").GetComponent<sceneChangerClick>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (openDoor.doorOpened && other.gameObject.CompareTag("Player"))
        {
            //incrementar nivel en game manager
            gameManager.SaveGame();
            sceneChangerClick.irAEscena("3Monolith");
        }
        else
        {
            print("Door locked");
        }
    }
}
